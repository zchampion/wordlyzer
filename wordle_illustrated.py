import argparse
import ast
import json
import os
import re

if 'rich' in os.environ and os.environ['rich']:
    from rich.console import Console
    console = Console()

from wordle_words import GOAL_WORDS

TERMINAL_COLOR_BASE = "bold "
TERMINAL_COLOR_LOCKED = f"{TERMINAL_COLOR_BASE}white on green"
TERMINAL_COLOR_PRESENT = f"{TERMINAL_COLOR_BASE}white on yellow"
TERMINAL_COLOR_ABSENT = f"{TERMINAL_COLOR_BASE}white on black"

def all_in(members, group):
    return all([member in group for member in members])


def all_not_in(members, group):
    return all([member not in group for member in members])



class WordleDetective:
    def __init__(self):
        self._locked_letters = ['' for _ in range(5)] # list of letters in position
        self._present_letters = [set() for _ in range(5)] # list of sets of letters
        self._absent_letters = [] # Just a list of letters
        self.clue_regex = r'.....'
    
    def flatten_present_letters(self):
        return [letter for set_i in self._present_letters for letter in set_i ]
    
    def digest_word(self, word, goal):
        for pos, letter in enumerate(word):
            if letter in goal and goal[pos] == letter:
                if self._locked_letters[pos] in ('', letter):
                    self._locked_letters[pos] = letter
                else:
                    raise ValueError(f"Tried to assign letter '{letter}' in position {pos}, but the letter "
                                     f"'{self._locked_letters[pos]}' is already there.")
            elif letter in goal:
                self._present_letters[pos].add(letter)
            else:
                self._absent_letters.append(letter)
        
        self.clue_regex = self.create_clue_regex()

    def create_pattern(self, word, goal):
        pattern = ''
        for pos, letter in enumerate(word):
            if letter in self._locked_letters:
                pattern += 'L'
            elif letter in self.flatten_present_letters():
                pattern += 'P'
            elif letter in self._absent_letters:
                pattern += 'A'
            else:
                self.digest_word(word, goal)
                return self.create_pattern(word, goal)
    
        return pattern

    def create_clue_regex(self):
        proto_regex = ['.' for _ in range(5)]
        # Put the not clauses in from the present letters
        for pos, letters in enumerate(self._present_letters):
            if len(self._present_letters[pos]) > 0:
                proto_regex[pos] = f"[^{''.join(self._present_letters[pos])}]"
                
        # Overwrite all that with the locked letters if there are any
        for pos, letter in enumerate(self._locked_letters):
            if self._locked_letters[pos] != '':
                proto_regex[pos] = letter
        
        return ''.join(proto_regex)

    def remaining_possibilities(self):
        remaining = [word for word in GOAL_WORDS if
                     all_in(self.flatten_present_letters(), word) and
                     all_not_in(self._absent_letters, word) and
                     re.match(self.clue_regex, word)]
        return remaining

def analyze_words(words, goal_word):
    """
    
    :param words: 
    :param goal_word: 
    :return: List of dictionaries with attributes of the word's analysis.
    """
    analysis = {}
    detective = WordleDetective()
    for word in words:
        detective.digest_word(word, goal_word)
        possibilities = detective.remaining_possibilities()
        analysis[word] = {
            'pattern': detective.create_pattern(word, goal_word),
            'possibilities': len(possibilities)
        }
    
    return analysis


def format_analysis_for_terminal(analysis):
    try:
        formatted_analysis = ''
        
        for word in analysis.keys():
            for i, letter in enumerate(word):
                color = TERMINAL_COLOR_LOCKED if analysis[word]['pattern'][i] == "L" else TERMINAL_COLOR_PRESENT if analysis[word]['pattern'][i] == "P" else TERMINAL_COLOR_ABSENT
                formatted_analysis += f"[{color}] {letter.upper()} [/{color}]"
            
            formatted_analysis += f" - {analysis[word]['possibilities']: >2,} possibilities remain\n"
        
        return formatted_analysis
    
    except Exception as e:
        print(f"ERROR: {e}", style='bold red')
        pretty_analysis = json.dumps(analysis, indent=2)
        return pretty_analysis


def format_analysis_for_sms(analysis):
    try:
        formatted_analysis = ''
        
        for word in analysis:
            formatted_analysis += f"{word.upper()} | {analysis[word]['possibilities']: >2,} remain\n"
        
        return formatted_analysis
    
    except Exception as e:
        print(f"ERROR: {e}", style='bold red')
        pretty_analysis = json.dumps(analysis)
        return pretty_analysis
        

def display_analysis(words, goal_word):
    analysis = analyze_words(words, goal_word)
    display = format_analysis_for_terminal(analysis)
    return display
    
    
def main(args):
    goal_word = args.goal
    if args.goal is None:
        goal_word = args.words[-1]
    
    return display_analysis(args.words, goal_word)

def sms_handler(event, context):
    print(f"Event received: {event}")

    for record in event['Records']:
        if "Sns" in record and 'Message' in record['Sns']:
            message = ast.literal_eval(record['Sns']['Message'])
            print(f"Message: {message}")
            words = message['messageBody'].split()
            analysis = analyze_words(words, words[-1])
            return format_analysis_for_sms(analysis)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('words', nargs='+', help='Words guessed.')
    parser.add_argument('-g', '--goal', default=None, help='Goal word of the day.')
    args = parser.parse_args()
    print(main(args))
