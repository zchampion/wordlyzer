from wordle_words import GOAL_WORDS, ALLOWED_WORDS
from string import ascii_lowercase, ascii_uppercase
import logging
from rich.logging import RichHandler

FORMAT = "%(message)s"
logging.basicConfig(
    level="NOTSET", format=FORMAT, datefmt="[%X]", handlers=[RichHandler()]
)

log = logging.getLogger("rich")

def score(word, ranks):
    wscore = 0
    for char in word.lower():
        assert char in ascii_lowercase, f"character {char} in {word} is invalid!"
        wscore += ranks.index(char)
    return wscore


def main():
    CDIST = {c: 0 for c in ascii_lowercase}

    # Get the frequency of each letter
    for c in ''.join(GOAL_WORDS):
        CDIST[c] += 1
    CDIST = sorted(CDIST.items(), reverse=True, key=lambda x: x[1])
    log.info(f"Scored Character Distribution:\n{CDIST}")

    WDIST = sorted({(word, score(word, [c[0] for c in CDIST])) for word in GOAL_WORDS}, reverse=True, key=lambda x: x[1])
    log.info(f"Scored Word Distribution:\n{WDIST}")


if __name__ == "__main__":
    main()
